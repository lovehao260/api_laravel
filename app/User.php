<?php

namespace App;

use App\Models\Chat;
use App\Models\Message;
use App\Models\Team;
use App\Notifications\ResetPassword;
use App\Notifications\VerifyEmail;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Tymon\JWTAuth\Contracts\JWTSubject;



class User extends Authenticatable implements JWTSubject,MustVerifyEmail
{
    use Notifiable , SpatialTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','tagline','about','user_name','avatar',
        'formatted_address','del_flg'
    ];
    protected $spatialFields = [
        'location'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail());
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function designs(){
        return $this->hasMany(Design::class);
    }
    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function teams(){
        return  $this->belongsToMany(Team::class,'team_users')->withTimestamps();
    }

    public function ownedTeams(){
        return $this->teams()->where('owner_id',$this->id);
    }

    public function isOwnerOfTeam($team){
        return (bool) $this->teams()
            ->where('id',$team->id)
            ->where('owner_id',$this->id)
            ->count();
    }

    // Relationships for chat messaging
    public function chats(){
        return $this->belongsToMany(Chat::class,'participants');
    }

    public function messages(){
        return $this->hasMany(Message::class);
    }

    public function getChatWithUser($user_id){
      $chat= $this->chats()->whereHas('participants',function ($query) use ($user_id){
            $query->where('user_id',$user_id);
        })->first();

      return $chat;
    }
}
