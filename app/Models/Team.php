<?php

namespace App\Models;

use App\Comment;
use App\Design;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable=[
     'id','name','owner_id','slug'
    ];

    protected
    protected static function boot()
    {
        parent::boot();
        //when team is created ,add current user as
        //team member

        static::created(function ($teams){
         // auth()->user()->teams()->attach($teams->id);
           $teams->members()->attach(auth()->id());
        });
        static::updated(function ($teams){
            // auth()->user()->teams()->attach($teams->id);
            $teams->members()->attach(auth()->id());
        });
        static::deleting(function ($teams){
            $teams->members()->sync([]);
        });
    }

    public function owner(){
        return $this->belongsTo(User::class,'owner_id');
    }

    public function members(){
        return $this->belongsToMany(User::class,'team_users')->withTimestamps();
    }

    public function designs(){
        $this->hasMany(Design::class);
    }

    public function hasUser(User $user){
        return $this->members()
            ->where('user_id',$user->id)
            ->first() ?true:false;
    }


}
