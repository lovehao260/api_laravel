<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use  \Illuminate\Validation;
use Tymon\JWTAuth\JWTGuard;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    public function attemptLogin(Request $request)
    {
        // attempt to isue a token to the user based on the login credentials
        $token = $this->guard()->attempt($this->credentials($request));
        if (! $token){
            return false;
        }
        //get the authenticated user
        $user = $this->guard()->user();

        if($user instanceof  MustVerifyEmail && !$user->hasVerifiedEmail()){
            return  false;
        }
        // set the user's token
        $this->guard()->setToken($token);
//        $request->has('remember');
        return $this->guard()->attempt(
            $this->credentials($request), $request->has('password')
        );
    }
    public function username()
    {
        $login = request()->input('login');
        $field = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'user_name';
        request()->merge([$field => $login]);
        return $field;
    }
    protected  function sendLoginResponse(Request $request){
        $this->clearLoginAttempts($request);
        // get the token from the authencation guard (JWT)
        $token =(string)$this->guard()->getToken();

        // exxtract the expiry date of cua the token
        $expiration = $this-> guard()->getPayLoad()->get('exp');
        return response() ->json([
            'token'=>$token,
            'token_type'=>'bearer',
            'expires_in'=>$expiration
        ]);
    }

    protected function  sendFailedLoginResponse(Request $request)
    {
        $user = $this->guard()->user();
        if($user instanceof  MustVerifyEmail && !$user->hasVerifiedEmail()){
            return  response()->json(["errors"=>[
                "verification"=>"you need to verify you email account"
            ]],422);
        }

        throw ValidationException::withMessages([
            $this->username()=>'Invalid credentials'
        ]);
    }


    public function logout(){
        $this->guard()->logout();
        return response()->json([
            'messages'=>'Logged out successfully !'
        ]);
    }
}
