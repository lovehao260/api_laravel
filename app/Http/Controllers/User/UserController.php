<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Repositories\Contracts\IUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;


class UserController extends Controller
{
    protected $users;

    public function __construct(IUser $users)
    {
        $this->users = $users;
    }

    public function index(){
        $users= $this->users->all();
        return UserResource::collection($users);
    }

    public function store(Request $request){
        $this->validate($request, [
            'user_name' => ['required', 'string', 'max:15','alpha_dash','unique:users,user_name'],
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'same:confirmPassword'],
        ]);
        $files = $request->file('avatar');

        if($files){
            $filename = time()."_". preg_replace('/\s+/','_',strtolower($files->getClientOriginalName()));
            Image::make($files)->save(public_path('storage/avatar/') . $filename);
        }else{
            $filename='';
        }
        $this->users->create([
            'user_name' => $request['user_name'],
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'avatar'=>$filename
        ]);
        return response()->json(null,200);
    }
    public function update($id,Request $request){
        $this->validate($request, [
            'user_name' => 'required', 'string', 'max:15','alpha_dash','unique:users,user_name'.$id,
            'name' => 'required', 'string', 'max:255',
            'email' => 'required', 'string', 'email', 'max:255', 'unique:users,email'.$id,
        ]);
        $files = $request->file('avatar');

        if($files){
            $filename = time()."_". preg_replace('/\s+/','_',strtolower($files->getClientOriginalName()));
            Image::make($files)->save(public_path('storage/avatar/') . $filename);
        }else{
            $filename='';
        }
        $this->users->update($id,[
            'user_name' => $request['user_name'],
            'name' => $request['name'],
            'email' => $request['email'],
            'avatar' => $filename,
        ]);
        return response()->json(null,200);
    }
    public function destroy($id){
        if ($id){
            $this->users->delete($id);
            return response()->json([
                'message' => "Delete record success ."
            ], 200);
        }
        return  response()->json(["errors"=>[
            "id"=>"ID not found"
        ]],404);
    }
    public function getMe(){
        if(auth()->check()){
            $user=auth()->user();
             return new UserResource($user);
//            return response()->json(["user"=>auth()->user()],200);
        }
        return response()->json(null,200);
    }


}
