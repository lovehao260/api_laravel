<?php

namespace App\Http\Controllers\Setting;

use App\Http\Resources\UserResource;

use App\Rules\CheckSamePassword;
use App\Rules\MatchOldPassword;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Grimzy\LaravelMysqlSpatial\Types\Polygon;
use Grimzy\LaravelMysqlSpatial\Types\LineString;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class SettingController extends Controller
{
    public function updateProfile(Request $request){
        $user = auth()->user();

        $this->validate($request,[
            'tagline'=>['required'],
            'name'=>['required'],
            'about'=>['required'],
            'location.latitude'=>['required','numeric','min:-90','max:90'],
            'location.longitude'=>['required','numeric','min:-180','max:180'],

        ]);

        $location = new Point( $request->location['latitude'], $request->location['longitude'], $srid = 0);
        $user->update([
            'name'=>$request->name,
            'location'=>  new Point(40.7484404, -73.9878441, 4326),	// (lat, lng, srid),
            'about'=>$request->about,
            'tagline'=>$request->tagline,

        ]);

        return new UserResource($user);
    }

    public function updatePassword(Request $request){
        $this->validate($request,[
            'current_password'=>['required', new MatchOldPassword],
            'password'=>['required','confirmed','min:6',new CheckSamePassword()],
        ]);
//         Validator::make($request->all(), [
//            'old_password' => [
//                'required', function ($attribute, $value, $fail) {
//                    if (!Hash::check($value, Auth::user()->password)) {
//                        $fail('Old Password didn\'t match');
//                    }
//                },
//            ],
//            'password' => 'required|min:6|different:old_password',
//            'cf_password' => 'required|min:6|same:password'
//        ]);
        $request->user()->update([
            'password'=>bcrypt($request->password)
        ]);

        return response()->json([
            "messages"=>"Update password success !!"
        ],200);
    }

    public function updateAvatar(Request $request)
    {
        $user = auth()->user();
        if ($request->get('avatar')) {
            $image = $request->avatar;  // your base64 encoded
            //Delete file old
            if (Storage::disk('public')->exists('avatar/', $user->avatar)) {
                File::delete('storage/avatar/' . $user->avatar);
            }
            $name = Str::uuid() . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            if(!Storage::exists('public/avatar/')){
                Storage::makeDirectory('public/avatar/', 0777, true, true);
            }
            Image::make($image)->save(public_path('storage/avatar/') . $name);
            $user->update([
                'avatar'=>$name,
            ]);
            return  response()->json(["avatar"=>$name],200);
        }
        return  response()->json(["errors"=>"can not image"],404);

    }
}
