<?php

namespace App\Http\Controllers\Chat;

use App\Events\ChatEventsPrivate;
use App\Http\Controllers\Controller;
use App\Http\Resources\ChatResource;
use App\Http\Resources\MessageResource;
use App\Repositories\Contracts\IChat;
use App\Repositories\Contracts\IMessage;

use App\Repositories\Eloquent\Criteria\{WithTrashed ,LatestFirst};
use Illuminate\Http\Request;

class ChatController extends Controller
{
    protected $chats;
    protected $messages;

    public function __construct(IChat $chats, IMessage $messages)
    {
        $this->chats = $chats;
        $this->messages = $messages;
    }

    public function sendMessage(Request $request)
    {
        //validate the request
        $this->validate($request, [
            'recipient' => ['required']
        ]);
        $recipient = $request->recipient;
        $user = auth()->user();
        $body = $request->body;
        // Check if there is an  existing chat
        //between the auth  user and the recipient
        $chat = $user->getChatWithUser($recipient);
        if (!$chat) {
            $chat = $this->chats->create([]);
            $this->chats->createParticipants($chat->id, [$user->id, $recipient]);
        }
        // add  the message to the chat
        $files = $request->file('attachment');

        if($files){
            $filename = time()."_". preg_replace('/\s+/','_',strtolower($files->getClientOriginalName()));
            $files->storeAs('public/uploads/attachments',$filename,'local');
        }else{
            $filename='';
        }
        $message = $this->messages->create([
            'user_id' => $user->id,
            'chat_id' => $chat->id,
            'attachment'=>$filename,
            'body' => $body,
            'last_read' => null
        ]);

        broadcast(new ChatEventsPrivate($request->chat_id, new MessageResource($message)))->toOthers();
        return new MessageResource($message);

    }

    //Get Chat for users
    public function getUserChats()
    {
        $chats = $this->chats->getUserChats(auth()->id());
        return ChatResource::collection($chats);
    }

    //Get messages for chat
    public function getChatMessages($id)
    {
//        $messages= $this->messages->findWhere('chat_id',$id)->withTrashed();
        $messages = $this->messages->
             withCriteria(new WithTrashed() , new LatestFirst())->findWhere('chat_id', $id);
        return MessageResource::collection($messages)->paginate(30);
    }

    //mark chat as read
    public function markAsRead($id)
    {
        $chat= $this->chats->find($id);
        $chat->markAsReadForUser(auth()->id());
        return response()->json([
            "message"=>'successful'
        ],200);
    }

    //delete message
    public function destroyMessage($id)
    {
       $message= $this->messages->find($id);
       $this->authorize('delete',$message);
        $message->delete();
    }

    public function realMessage(Request $request){
        $user = auth()->user();
        broadcast(new ChatEventsPrivate( $request->message))->toOthers();
    }
}
