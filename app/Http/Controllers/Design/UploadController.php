<?php

namespace App\Http\Controllers\Design;

use App\Http\Controllers\Controller;
use App\Jobs\UploadImage;
use Illuminate\Http\Request;

class UploadController extends Controller
{


    public function upload(Request $request){
        //validate upload file image
        $this->validate($request,[
            'image'=>['required','mimes:jpeg,gif,bmp,png','max:2048']
        ]);
        // get the image
        $image = $request->file('image');
        $image_path= $image->getPathname();
        // get the original file name and replace any spaces with_
        // Business Cards.png = timestamo()-business_cards.png
        $filename = time()."_". preg_replace('/\s+/','_',strtolower($image->getClientOriginalName()));
        $tmp=$image->storeAs('uploads/original',$filename,'tmp');

        //Created the database record for the design
        $design = auth()->user()->designs()->create([
            'image'=>$filename,
            'disk'=>config('site.upload_disk')
        ]);

        $this->dispatch(new UploadImage($design));
        return response()->json($design,200);
    }
}
