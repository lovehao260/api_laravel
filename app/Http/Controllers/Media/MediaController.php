<?php

namespace App\Http\Controllers\Media;

use App\Events\CreatedContentEvent;
use App\Http\Controllers\Controller;
use App\Http\Resources\FileResource;
use App\Http\Resources\FolderResource;
use App\Models\MediaFile;
use App\Models\MediaFolder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class MediaController extends Controller
{
    public function index(Request $request,$id){
//        if($request->loadType==='all'){
//            $query_folders = MediaFolder::where('parent_id', $id);
//            $query_files = MediaFile::where('folder_id', $id);
//        }
//
//        if($request->loadType==='trash'){
//            $query_folders = MediaFolder::onlyTrashed();
//            $query_files = MediaFile::onlyTrashed();
//        }
//        if ($request->search) {
//            $query_folders = $query_folders->where('name', 'LIKE', '%' . $request->search . '%');
//            $query_files = $query_files->where('name', 'LIKE', '%' . $request->search . '%');
//        }
//        $folders = FolderResource::collection($query_folders->get());
//        $files = FileResource::collection($query_files->get());
        return [
            'folders' => $request,
//            'files' => $files,
        ];
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function storeFolder(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:media_folders'
        ]);
        $folder =new MediaFolder;
        $folder->create([
            'name' => $request->name,
            'slug' => Str::slug($request->name),
            'parent_id' => $request->folder_id,
            'user_id' => 1,
        ]);
//        event(new CreatedContentEvent($request->all(), $folder, config('general.log.creat')));
        if ( $request->folder_id !=0){
            $folder_parent=MediaFolder::find($request->folder_id);
            $folder_path=$folder_parent->slug.'/'. $folder->slug;
        }else{
            $folder_path= $folder->slug;
        }
        if(! Storage::exists( $folder_path)){
            Storage::makeDirectory($folder_path);
        }
        return response()->json([
            "messages" =>trans('media.store')
        ], 200);
    }
}
