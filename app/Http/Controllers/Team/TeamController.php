<?php

namespace App\Http\Controllers\Team;

use App\Http\Controllers\Controller;
use App\Http\Resources\TeamResource;
use App\Repositories\Contracts\ITeam;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TeamController extends Controller
{

    protected  $teams;


    public function __construct(ITeam $teams)
    {
        $this->teams = $teams;
    }

    public function index(Request $request){

    }

    public function store(Request $request){
        $this->validate($request,[
            'name'=>['required','string','max:80','unique:teams,name']
        ]);

        $team=$this->teams->create([
            'owner_id'=>auth()->id(),
            'name'=>$request->name,
            'slug'=>Str::slug($request->name)
        ]);

        return new TeamResource($team);
    }

    public function update(Request $request,$id){


        $this->validate($request,[
                "name"=>['required','string','max:80','unique:teams,name,'.$id]
        ]);

        $team= $this->teams->update($id,[
            'name'=>$request->name,
            'slug'=>Str::slug($request->name)
        ]);
        $team= $this->teams->find($id);
        $this->authorize('update',$team);
        return new TeamResource($team);
    }
    /**
     * Get team by slug for Public views
     */
    public function findBySlug($slug){

    }

    /**
     * Find a team by its ID
    */
    public function findById($id){
        $team= $this->teams->find($id);
        return new TeamResource($team);
    }
    /**
     * Get the teams that the current user belongs to
     */
    public function fetchUserTeams(){
        $teams= $this->teams->fetchUserTeams();
        return TeamResource::collection($teams);
    }
    public function destroy($id){

    }
}
