<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
//        return parent::toArray($request);
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'user_name'=>$this->user_name,
            'avatar'=>$this->avatar?asset('storage/avatar/' .$this->avatar):'',
            'email'=>$this->email,
            $this->mergeWhen(auth()->check() && auth()->id()=== $this->id,[
                'verify'=>'yes',
            ]),
            'designs'=>DesignResource::collection(
                $this->whenLoaded('designs')
            ),
            'created_dates'=>[
                'created_at_human'=>$this->created_at->diffForHumans(),
                'created_at'=>$this->created_at,
            ],
            'formatted_address'=>$this->formatted_address,
            'tagline'=>$this->tagline,
            'about'=>$this->about,
            'location'=>$this->location,
            'del_flg'=>$this->del_flg,
        ];
    }
}
