<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DesignResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,

            'title'=>$this->title,
            'slug'=>$this->slug,
            'description'=>$this->description,
            'is_live'=>$this->is_live,
            'image'=>$this->images,
            'like_count'=>$this->likes()->count(),
            'tag_list'=>[
                'tags'=>$this->tagArray,
                'normalized'=>$this->tagArrayNormalized
            ],
            'created_dates'=>[
                'created_at_human'=>$this->created_at->diffForHumans(),
                'created_at'=>$this->created_at,
            ],
            'updated_dates'=>[
                'updated_at_human'=>$this->created_at->diffForHumans(),
                'updated_at'=>$this->created_at,
            ],

            'comments'=> CommentResource::collection($this->whenLoaded('comments')),
            'user'=>new UserResource($this->whenLoaded('user')),
        ];
    }
}
