<?php

namespace App;

use App\Models\Traits\Likeable;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use Likeable;
   protected $fillable=[
       'id','body','user_id'
   ];

   public  function commentable(){
        return $this->morphTo();
   }

   public function  user(){
       return $this->belongsTo(User::class);
   }
}
