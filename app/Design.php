<?php

namespace App;

use App\Models\Traits\Likeable;
use Cviebrock\EloquentTaggable\Taggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Design extends Model
{
    use Taggable ,Likeable;
    protected $fillable=[
        'user_id',
        'team_id',
        'image',
        'disk',
        'title',
        'description',
        'slug',
        'close_to_comment',
        'is_live',
        'upload_successful',

    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function comments(){
        return $this->morphMany(Comment::class,'commentable')
            ->orderBy('created_at','asc');
    }



    public function getImagesAttribute(){
            return [
                'thumbnail'=>$this->getImagesPath('thumbnail'),
                'large'=>$this->getImagesPath('large'),
                'original'=>$this->getImagesPath('original'),
            ];
    }

    public function getImagesPath($size){
        return Storage::disk($this->disk)->url("upload/designs/{$size}/".$this->image);

    }
}
