<?php
namespace App\Repositories\Contracts;

interface IBase{
    public function all();
    public function find($id);
    public function findWhere($column, $value);
    public function finWhereFirst($column,$value);
    public function pagination($perPage);
    public function create(array $data);
    public function update($id, array $data);
    public function delete($id);
}
