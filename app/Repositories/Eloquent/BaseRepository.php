<?php
namespace App\Repositories\Eloquent;
use App\Exceptions\ModelNotDefined;
use App\Repositories\Contracts\IBase;
use App\Repositories\Criteria\ICriteria;
use Illuminate\Support\Arr;

abstract class BaseRepository implements IBase ,ICriteria {

    protected  $model;
    public function __construct()
    {
        $this->model= $this->getModelClass();
    }

    public function withCriteria(...$criteria)
    {
        $criteria= Arr::flatten($criteria);
        foreach ($criteria as $criterion){
            $this->model = $criterion->apply($this->model);
        }
        return $this;
    }

    public function all(){
        return $this->model->get();
    }

    public function find($id){
        return $this->model->findOrFail($id);
    }

    public function findWhere($column,$value){
        return $this->model->where($column,$value)->get();
    }

    public function finWhereFirst($column,$value){
        return $this->model->where($column,$value)->firstOrFail();
    }

    /**
     * Retrieve all data of repository, paginated
     * @param int $perPage
     */
    public function pagination($perPage=10){
        return $this->model->paginate($perPage);
    }

    public function create(array  $data){
        return $this->model->create($data);
    }

    public function update($id,array  $data){
        $record = $this->find($id);
        return $record->update($data);

    }

    public function delete($id){
        $record = $this->find($id);
        return $record->delete();
    }


    protected function getModelClass(){
        if(!method_exists($this,'model')){
            throw new ModelNotDefined ();

        }
        return app()->make($this->model());
    }
}
