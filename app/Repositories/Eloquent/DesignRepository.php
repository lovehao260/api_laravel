<?php
namespace App\Repositories\Eloquent;


use App\Design;
use App\Repositories\Contracts\IDesign;
use Illuminate\Support\Arr;

class  DesignRepository extends BaseRepository implements IDesign {

    public function model(){
        return Design::class ;
    }

    public function applyTags($id,array $data){
        $design= $this->find($id);
        $design->retag($data);
    }

    public function allLive(){
        return $this->model->where('is_live',true)->get();
    }

    public function addComment($designId,array $data){
        // get the design for witch we want to create a comment
        $design = $this->find($designId);
        // create the comment for the design
        $comment = $design->comments()->create($data);
    }

    public function like($id){
        $design = $this->model->findOrFail($id);
        if($design->isLikedByUser(auth()->id())){
            $design->unlike();
        }else{
            $design->like();
        }
    }

    public function isLikedByUser($id){
        $design =$this->model->findOrFail($id);
        return $design->isLikedByUser(auth()->id());
    }
}
