import './bootstrap';
import Vue from 'vue';

import App from './components/layout/App'
import VueAuth from '@websanova/vue-auth';
import axios from 'axios';
import VueAxios from 'vue-axios';
axios.defaults.baseURL = `http://api_laravel.test/api`
import VueRouter from 'vue-router';

Vue.use(VueAxios, axios);
import router from './config/router';
import store from "./store/store";
Vue.router = router;
App.router = Vue.router;
Vue.prototype.$urlBase ='http://api_laravel.test'
import auth from './config/auth';
Vue.use(VueAuth, auth);
Vue.use(VueRouter);

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
new Vue({
    router,
    store,
    render: (h) => h(App),
}).$mount('#app');
