import VueRouter from 'vue-router';
import Login from '../components/fontend/pages/auth/Login';
import Register from '../components/fontend/pages/auth/Register';
import SendMailPassword from '../components/fontend/pages/auth/SendMailPassword';
import ResetPassword from '../components/fontend/pages/auth/ResetPassword';
import Verification from '../components/fontend/pages/auth/Verification';
import Verify from '../components/fontend/pages/auth/Verify';
import Home from '../components/fontend/pages/designs/create';
import Dashboard from '../components/backend/pages/Daskboard';
import CreateDesign from '../components/fontend/pages/designs/create';
import EditDesign from '../components/fontend/pages/designs/edit';

import IndexChat from '../components/backend/pages/chat/IndexChat';

const routes = [
    // path for determined page
    /****Auth**************/
    {
        path: '/api/verification/verify/:id',
        component: Verification,
        meta: {
            auth: false
        }
    },
    {
        path: '/verification/resend',
        component: Verify,
        meta: {
            auth: false
        }
    },
    {
        path: '/',
        name: 'home',
        component: Home,
        meta: {
            auth: false
        }
    },{
        path: '/register',
        name: 'register',
        component: Register,
        meta: {
            auth: false
        }
    },{
        path: '/login',
        name: 'login',
        component: Login,
        meta: {
            auth: false
        }
    },
    {
        path: '/password/email',
        name: 'forgot.password',
        component: SendMailPassword,
        meta: {
            auth: false
        }
    },
    {
        path: '/api/password/reset/:token',
        component: ResetPassword,
        meta: {
            auth: false
        }
    },
    {
        path: '/dashboard',
        name: 'dashboard',
        component: Dashboard,
        meta: {
            auth: true
        }
    },
    /****page**************/

    {
        path: '/upload',
        name: 'designs.upload',
        component: CreateDesign,
        meta: {
            auth: true
        }
    },

    {
        path: '/designs/:id/edit',
        name: 'designs.edit',
        component: EditDesign,
        meta: {
            auth: true
        }
    },
    //Chat -----------------------------------
    {
        path: '/chat',
        name: 'chat',
        component: IndexChat,
        meta: {
            auth: true
        }
    },

];


const router = new VueRouter({
    mode: 'history',
    routes: routes
});

export default router;
