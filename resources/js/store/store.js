import Vue from "vue";
import Vuex from "vuex";
import * as moment from "moment";
Vue.use(Vuex);
export default  new Vuex.Store({
    state: {
        messages: [],
        to_user:'',
        users:[],
        selected_id:'',
        nightMode:localStorage.getItem('nightMode')
    },
    mutations: {
        setMessageId(state, payload) {
            state.selected_id = payload;
        },
        setNightMode(state, payload) {
            state.nightMode = payload;
        },
        updateUser(state, payload){
            const { body, attachment,order_by,created_at_human,user_id,sender_id } = payload
            const person = state.users.find(p => p.user_id === user_id)
            person.body = body
            person.sender_id = sender_id
            person.attachment = attachment
            person.created_at_human = created_at_human
            person.order_by = order_by
        }
    },
    actions: {
        orderUserChat() {

        }
    }
});
