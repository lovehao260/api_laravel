import * as moment from "moment";

export const formatMessage = {
        methods: {
            renderHiddenAvatar(index, all, item) {
                if (index === 0 && all.length === 1) {
                    return false;
                } else if (
                    index !== 0 &&
                    item.sender.id !== all[index - 1].sender.id &&
                    index === all.length - 1
                ) {
                    return false;
                } else if (index !== 0 && item.sender.id === all[index - 1].sender.id &&
                    moment(item.dates.created_at).format("YYYY-MM-DD") === moment(all[index - 1].dates.created_at).format("YYYY-MM-DD")) {
                    return true;
                } else if (all.length > 0 && index === all.length - 1) {
                    return true;
                }
                return false;
            },

            checkMyMessageRadius(index, conversationAll, item) {

                if (index === 0) {
                    if (conversationAll.length > 1 && item.sender.id === conversationAll[index + 1].sender.id ) {
                        return 'border-bottom-right-radius: 2px';
                    }else {
                        return 'border-radius: 15px';
                    }

                } else if (index === conversationAll.length - 1) {
                    if (item.sender.id  === conversationAll[index - 1].sender.id  ) {
                        return 'border-top-right-radius: 2px';
                    } else {
                        return 'border-radius: 15px';
                    }
                } else if (index > 0 && index < conversationAll.length - 1) {
                    if (
                        item.sender.id  === conversationAll[index + 1].sender.id  &&
                        item.sender.id  !== conversationAll[index - 1].sender.id

                    ) {
                        return 'border-bottom-right-radius: 2px';
                    }
                    else if(item.attachment &&     item.sender.id  !== conversationAll[index - 1].sender.id){
                        return 'border-top-right-radius: 2px '
                    }
                    else if (
                        item.sender.id  !== conversationAll[index + 1].sender.id  &&
                        item.sender.id  === conversationAll[index - 1].sender.id
                    ) {
                        return 'border-top-right-radius: 2px';
                    } else if (
                        item.sender.id  !== conversationAll[index + 1].sender.id  &&
                        item.sender.id  !== conversationAll[index - 1].sender.id
                    ) {
                        return 'border-radius: 15px ';
                    }
                    else if (
                        moment(item.dates.created_at).format("YYYY-MM-DD") !== moment(conversationAll[index - 1].dates.created_at).format("YYYY-MM-DD")) {
                        return 'border-bottom-right-radius: 2px'
                    }
                    else if (
                        moment(item.dates.created_at).format("YYYY-MM-DD") !== moment(conversationAll[index + 1].dates.created_at).format("YYYY-MM-DD")) {
                        return 'border-top-right-radius: 2px'
                    }

                    else {
                        return 'border-bottom-right-radius: 2px ;border-top-right-radius: 2px';
                    }
                }

                else {
                    return 'border-bottom-right-radius: 2px ;border-top-right-radius: 2px';
                }

            },
            checkYourMessageRadius(index, conversationAll, item) {
                if (index === 0) {
                    if (conversationAll.length > 1 && item.sender.id === conversationAll[index + 1].sender.id) {
                        return 'border-bottom-left-radius: 2px';
                    } else {
                        return 'border-radius: 15px';
                    }

                } else if (index === conversationAll.length - 1) {
                    if (item.sender.id === conversationAll[index - 1].sender.id) {
                        return 'border-top-left-radius: 2px';
                    } else {
                        return 'border-radius: 15px';
                    }
                } else if (index > 0 && index < conversationAll.length - 1) {
                    if (
                        item.sender.id === conversationAll[index + 1].sender.id &&
                        item.sender.id !== conversationAll[index - 1].sender.id
                    ) {
                        return 'border-bottom-left-radius: 2px';
                    }
                    else if(item.attachment &&     item.sender.id  !== conversationAll[index - 1].sender.id){
                        return 'border-top-left-radius: 2px '
                    }
                    else if (
                        item.sender.id !== conversationAll[index + 1].sender.id &&
                        item.sender.id === conversationAll[index - 1].sender.id
                    ) {
                        return 'border-top-left-radius: 2px';
                    } else if (
                        item.sender.id !== conversationAll[index + 1].sender.id &&
                        item.sender.id !== conversationAll[index - 1].sender.id
                    )
                    {
                        return 'border-radius: 15px';
                    }
                    else if (
                        moment(item.dates.created_at).format("YYYY-MM-DD") !== moment(conversationAll[index - 1].dates.created_at).format("YYYY-MM-DD")) {
                        return 'border-top-left-radius: 15px ;border-bottom-left-radius: 2px ;'
                    }
                    else if (
                        moment(item.dates.created_at).format("YYYY-MM-DD") !== moment(conversationAll[index + 1].dates.created_at).format("YYYY-MM-DD")) {
                        return 'border-top-left-radius: 2px ;border-bottom-left-radius: 15px ; '
                    }
                    else {
                        return 'border-top-left-radius: 2px ;border-bottom-left-radius: 2px';
                    }
                } else {
                    return 'border-top-left-radius: 2px ;border-bottom-left-radius: 2px';
                }
            },
            checkLastMessage(index, conversationAll, item) {
                if (
                    conversationAll.length > 1 &&
                    index === 0 &&
                    item.senderId === conversationAll[index + 1].senderId
                ) {
                    return true;
                } else if (index === 0) {
                    return true;
                } else {
                    return false;
                }
            }
            ,
            showDate(index, conversationAll, item) {
                if (index === 0) {
                    return false;
                } else if (
                    conversationAll.length > 1 &&
                    index < conversationAll.length - 1 &&
                    moment(item.dates.created_at).format("YYYY-MM-DD") !== moment(conversationAll[index + 1].dates.created_at).format("YYYY-MM-DD")
                ) {
                    return true;
                } else {
                    return false;
                }
            }
            ,
            checkYourMessage(index, conversationAll, item) {
                if (
                    conversationAll.length > 1 &&
                    index === 0 &&
                    item.sender.id !== conversationAll[index + 1].sender.id
                ) {
                    return 'margin-top: 10px';
                } else if (conversationAll.length > 1 && index === 0) {
                    return 'margin-top: 10px';
                } else if (conversationAll.length === 1 && index === 0) {
                    return 'margin-top: 10px';
                } else if (
                    index < conversationAll.length - 1 &&
                    item.sender.id === conversationAll[index + 1].sender.id
                ) {
                    return 'margin: 10px';

                } else {
                    return 'margin-bottom: 10px';
                }
            },

            checkMyMessage(index, conversationAll, item) {
                if (
                    index > 0 &&
                    conversationAll.length > 1 &&
                    index < conversationAll.length - 1 &&
                    item.sender.id !== conversationAll[index + 1].sender.id &&
                    item.sender.id === conversationAll[index - 1].sender.id
                ) {
                    return 'margin-bottom: 10px';
                } else if (
                    index > 0 &&
                    conversationAll.length > 1 &&
                    index < conversationAll.length - 1 &&
                    item.sender.id !== conversationAll[index + 1].sender.id &&
                    item.sender.id !== conversationAll[index - 1].sender.id
                ) {
                    return 'margin-top: 10px ; margin-bottom: 10px';
                } else if (
                    conversationAll.length > 1 &&
                    index === 0 &&
                    item.sender.id === conversationAll[index + 1].sender.id
                ) {
                    return 'margin-top: 10px';
                } else if (index === 0) {
                    return 'margin-top: 10px';
                } else if (index > 0 && item.sender.id=== conversationAll[index - 1].sender.id) {
                    return 'margin: 0 ; border-top-right-radius: 2px';

                } else {
                    return 'margin-top: 10px';
                }
            },
            checkYourMessageName(index, conversationAll, item) {

                if (index === 0) {
                    return true;
                }
                if (
                    index > 0 &&
                    index < conversationAll.length - 1 &&
                    item.sender.id !== conversationAll[index - 1].sender.id
                ) {
                    return true;
                } else if (index === conversationAll.length - 1 &&
                    item.sender.id  !== conversationAll[index - 1].sender.id ) {
                    return true;
                } else {
                    return false;
                }
            }
            ,
            checkFileImage(file){
                if(file.match(/\.(jpeg|jpg|gif|png)$/) != null){
                    return 'image'
                }else {
                    return 'file'
                }
            }
        }
    }
;
