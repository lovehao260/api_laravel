---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://api_laravel.test/docs/collection.json)

<!-- END_INFO -->

#general


<!-- START_66df3678904adde969490f2278b8f47f -->
## Authenticate the request for channel access.

> Example request:

```bash
curl -X GET \
    -G "https://api_laravel.test/broadcasting/auth" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/broadcasting/auth"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET broadcasting/auth`

`POST broadcasting/auth`


<!-- END_66df3678904adde969490f2278b8f47f -->

<!-- START_c3fa189a6c95ca36ad6ac4791a873d23 -->
## Handle a login request to the application.

> Example request:

```bash
curl -X POST \
    "https://api_laravel.test/api/login" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/login"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/login`


<!-- END_c3fa189a6c95ca36ad6ac4791a873d23 -->

<!-- START_d7b7952e7fdddc07c978c9bdaf757acf -->
## Handle a registration request for the application.

> Example request:

```bash
curl -X POST \
    "https://api_laravel.test/api/register" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/register"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/register`


<!-- END_d7b7952e7fdddc07c978c9bdaf757acf -->

<!-- START_407479d38b62d51d7386951172eac599 -->
## api/verification/verify/{user}
> Example request:

```bash
curl -X POST \
    "https://api_laravel.test/api/verification/verify/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/verification/verify/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/verification/verify/{user}`


<!-- END_407479d38b62d51d7386951172eac599 -->

<!-- START_6332a19a7e88e2e5b9699f431bc38352 -->
## api/verification/resend
> Example request:

```bash
curl -X POST \
    "https://api_laravel.test/api/verification/resend" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/verification/resend"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/verification/resend`


<!-- END_6332a19a7e88e2e5b9699f431bc38352 -->

<!-- START_b7802a3a2092f162a21dc668479801f4 -->
## Send a reset link to the given user.

> Example request:

```bash
curl -X POST \
    "https://api_laravel.test/api/password/email" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/password/email"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/password/email`


<!-- END_b7802a3a2092f162a21dc668479801f4 -->

<!-- START_8ad860d24dc1cc6dac772d99135ad13e -->
## Reset the given user&#039;s password.

> Example request:

```bash
curl -X POST \
    "https://api_laravel.test/api/password/reset" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/password/reset"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/password/reset`


<!-- END_8ad860d24dc1cc6dac772d99135ad13e -->

<!-- START_61739f3220a224b34228600649230ad1 -->
## api/logout
> Example request:

```bash
curl -X POST \
    "https://api_laravel.test/api/logout" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/logout"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/logout`


<!-- END_61739f3220a224b34228600649230ad1 -->

<!-- START_b19e2ecbb41b5fa6802edaf581aab5f6 -->
## api/me
> Example request:

```bash
curl -X GET \
    -G "https://api_laravel.test/api/me" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/me"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/me`


<!-- END_b19e2ecbb41b5fa6802edaf581aab5f6 -->

<!-- START_fc1e4f6a697e3c48257de845299b71d5 -->
## api/users
> Example request:

```bash
curl -X GET \
    -G "https://api_laravel.test/api/users" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/users"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/users`


<!-- END_fc1e4f6a697e3c48257de845299b71d5 -->

<!-- START_eb042057a469fa8876c4e036a36e7ca3 -->
## api/users/store
> Example request:

```bash
curl -X POST \
    "https://api_laravel.test/api/users/store" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/users/store"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/users/store`


<!-- END_eb042057a469fa8876c4e036a36e7ca3 -->

<!-- START_e6a42516030016ca5b0524ae95b7c9c6 -->
## api/users/update/{id}
> Example request:

```bash
curl -X POST \
    "https://api_laravel.test/api/users/update/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/users/update/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/users/update/{id}`


<!-- END_e6a42516030016ca5b0524ae95b7c9c6 -->

<!-- START_53992c1cf558463c587e64c309a1053b -->
## api/users/destroy/{id}
> Example request:

```bash
curl -X DELETE \
    "https://api_laravel.test/api/users/destroy/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/users/destroy/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/users/destroy/{id}`


<!-- END_53992c1cf558463c587e64c309a1053b -->

<!-- START_7e636145846e01d877e2b42968194cc5 -->
## api/setting/profile
> Example request:

```bash
curl -X PUT \
    "https://api_laravel.test/api/setting/profile" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/setting/profile"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/setting/profile`


<!-- END_7e636145846e01d877e2b42968194cc5 -->

<!-- START_e53b01d9f9975ea490ded372534d52fb -->
## api/setting/password
> Example request:

```bash
curl -X PUT \
    "https://api_laravel.test/api/setting/password" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/setting/password"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/setting/password`


<!-- END_e53b01d9f9975ea490ded372534d52fb -->

<!-- START_ee99d866ed1d810f242bef70ffb4a62b -->
## api/setting/avatar
> Example request:

```bash
curl -X PUT \
    "https://api_laravel.test/api/setting/avatar" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/setting/avatar"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/setting/avatar`


<!-- END_ee99d866ed1d810f242bef70ffb4a62b -->

<!-- START_d1d3c257677c19fdd493dab09c9abfd9 -->
## api/designs
> Example request:

```bash
curl -X GET \
    -G "https://api_laravel.test/api/designs" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/designs"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/designs`


<!-- END_d1d3c257677c19fdd493dab09c9abfd9 -->

<!-- START_48141d7af90b36408c0caa07cdd4fc42 -->
## api/designs/detail/{id}
> Example request:

```bash
curl -X GET \
    -G "https://api_laravel.test/api/designs/detail/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/designs/detail/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/designs/detail/{id}`


<!-- END_48141d7af90b36408c0caa07cdd4fc42 -->

<!-- START_27f83682a083b16ef7348940e29871a7 -->
## api/designs/upload
> Example request:

```bash
curl -X POST \
    "https://api_laravel.test/api/designs/upload" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/designs/upload"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/designs/upload`


<!-- END_27f83682a083b16ef7348940e29871a7 -->

<!-- START_75b99370925dec8b910735d5dc84ff3e -->
## api/designs/{id}
> Example request:

```bash
curl -X PUT \
    "https://api_laravel.test/api/designs/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/designs/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/designs/{id}`


<!-- END_75b99370925dec8b910735d5dc84ff3e -->

<!-- START_f1e39148a5e13c6389cde354577c141a -->
## api/designs/{id}
> Example request:

```bash
curl -X DELETE \
    "https://api_laravel.test/api/designs/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/designs/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/designs/{id}`


<!-- END_f1e39148a5e13c6389cde354577c141a -->

<!-- START_8c7b372e97bdade0b01a5b9f1732c6f6 -->
## api/designs/search
> Example request:

```bash
curl -X GET \
    -G "https://api_laravel.test/api/designs/search" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/designs/search"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/designs/search`


<!-- END_8c7b372e97bdade0b01a5b9f1732c6f6 -->

<!-- START_d7964683c772b6a6aacb160e892869d8 -->
## api/designs/{id}/like
> Example request:

```bash
curl -X POST \
    "https://api_laravel.test/api/designs/1/like" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/designs/1/like"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/designs/{id}/like`


<!-- END_d7964683c772b6a6aacb160e892869d8 -->

<!-- START_32a399e239cdeade281ae6867c43f388 -->
## api/designs/{id}/liked
> Example request:

```bash
curl -X GET \
    -G "https://api_laravel.test/api/designs/1/liked" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/designs/1/liked"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/designs/{id}/liked`


<!-- END_32a399e239cdeade281ae6867c43f388 -->

<!-- START_e528f2b1fdcca5d33b2b3c5b5846c980 -->
## api/designs/{id}/comments
> Example request:

```bash
curl -X POST \
    "https://api_laravel.test/api/designs/1/comments" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/designs/1/comments"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/designs/{id}/comments`


<!-- END_e528f2b1fdcca5d33b2b3c5b5846c980 -->

<!-- START_3b7b6180f540f59e08a542df56f040ba -->
## api/comments/{id}
> Example request:

```bash
curl -X PUT \
    "https://api_laravel.test/api/comments/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/comments/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/comments/{id}`


<!-- END_3b7b6180f540f59e08a542df56f040ba -->

<!-- START_a446ee5cc043f690570906c492c40786 -->
## api/comments/{id}
> Example request:

```bash
curl -X DELETE \
    "https://api_laravel.test/api/comments/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/comments/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/comments/{id}`


<!-- END_a446ee5cc043f690570906c492c40786 -->

<!-- START_47a6d03c132411b782ac114472f71770 -->
## api/teams
> Example request:

```bash
curl -X GET \
    -G "https://api_laravel.test/api/teams" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/teams"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET api/teams`


<!-- END_47a6d03c132411b782ac114472f71770 -->

<!-- START_9a818b3c6cf54f86bad07a2e0bb61de1 -->
## Get the teams that the current user belongs to

> Example request:

```bash
curl -X GET \
    -G "https://api_laravel.test/api/users/teams" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/users/teams"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET api/users/teams`


<!-- END_9a818b3c6cf54f86bad07a2e0bb61de1 -->

<!-- START_bcf0ab089216e771774446e77bf0c5e5 -->
## api/teams
> Example request:

```bash
curl -X POST \
    "https://api_laravel.test/api/teams" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/teams"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/teams`


<!-- END_bcf0ab089216e771774446e77bf0c5e5 -->

<!-- START_104d86cdfc6ce23098e789da9371cd7d -->
## Find a team by its ID

> Example request:

```bash
curl -X GET \
    -G "https://api_laravel.test/api/teams/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/teams/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET api/teams/{id}`


<!-- END_104d86cdfc6ce23098e789da9371cd7d -->

<!-- START_b0527bfdc251cef7125a422def3021e0 -->
## api/teams/{id}
> Example request:

```bash
curl -X PUT \
    "https://api_laravel.test/api/teams/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/teams/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/teams/{id}`


<!-- END_b0527bfdc251cef7125a422def3021e0 -->

<!-- START_1006dd73624959845b5f083138fdbca8 -->
## api/teams/{id}
> Example request:

```bash
curl -X DELETE \
    "https://api_laravel.test/api/teams/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/teams/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/teams/{id}`


<!-- END_1006dd73624959845b5f083138fdbca8 -->

<!-- START_2b6e5a4b188cb183c7e59558cce36cb6 -->
## api/user
> Example request:

```bash
curl -X GET \
    -G "https://api_laravel.test/api/user" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/user"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/user`


<!-- END_2b6e5a4b188cb183c7e59558cce36cb6 -->

<!-- START_4f1b3a5b77339c79c0fcbf0c34411d1a -->
## api/user/upload
> Example request:

```bash
curl -X POST \
    "https://api_laravel.test/api/user/upload" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/user/upload"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/user/upload`


<!-- END_4f1b3a5b77339c79c0fcbf0c34411d1a -->

<!-- START_538c59bd7094f21614fa40efbc87039d -->
## api/user/{id}
> Example request:

```bash
curl -X PUT \
    "https://api_laravel.test/api/user/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/user/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/user/{id}`


<!-- END_538c59bd7094f21614fa40efbc87039d -->

<!-- START_a1ef15db35f08591deb485d3c5fb9a31 -->
## api/user/{id}
> Example request:

```bash
curl -X DELETE \
    "https://api_laravel.test/api/user/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/user/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/user/{id}`


<!-- END_a1ef15db35f08591deb485d3c5fb9a31 -->

<!-- START_907b92273edeabd0cf5c2a1dc516166d -->
## api/chats
> Example request:

```bash
curl -X POST \
    "https://api_laravel.test/api/chats" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/chats"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/chats`


<!-- END_907b92273edeabd0cf5c2a1dc516166d -->

<!-- START_cb57fd9b8379b4485e354a0acf65a50f -->
## api/chats
> Example request:

```bash
curl -X GET \
    -G "https://api_laravel.test/api/chats" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/chats"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/chats`


<!-- END_cb57fd9b8379b4485e354a0acf65a50f -->

<!-- START_957e1d10b45c20efe17a2b2908981a01 -->
## api/chats/{id}/messages
> Example request:

```bash
curl -X GET \
    -G "https://api_laravel.test/api/chats/1/messages" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/chats/1/messages"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/chats/{id}/messages`


<!-- END_957e1d10b45c20efe17a2b2908981a01 -->

<!-- START_b5ceca7be6aac43bc272a47f62915fd4 -->
## api/chats/{id}/markAsRead
> Example request:

```bash
curl -X PUT \
    "https://api_laravel.test/api/chats/1/markAsRead" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/chats/1/markAsRead"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/chats/{id}/markAsRead`


<!-- END_b5ceca7be6aac43bc272a47f62915fd4 -->

<!-- START_f9ebd7defccbeb31b9451c8ecaa08e2e -->
## api/messages/{id}
> Example request:

```bash
curl -X DELETE \
    "https://api_laravel.test/api/messages/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/messages/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/messages/{id}`


<!-- END_f9ebd7defccbeb31b9451c8ecaa08e2e -->

<!-- START_dcf82b9127a7b0c6fc35e055d7d806bb -->
## api/chats/real-time-message
> Example request:

```bash
curl -X POST \
    "https://api_laravel.test/api/chats/real-time-message" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/chats/real-time-message"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/chats/real-time-message`


<!-- END_dcf82b9127a7b0c6fc35e055d7d806bb -->

<!-- START_75d9e85049becd23a40b2c46323ea39f -->
## api/medias/{id}
> Example request:

```bash
curl -X GET \
    -G "https://api_laravel.test/api/medias/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/api/medias/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/medias/{id}`


<!-- END_75d9e85049becd23a40b2c46323ea39f -->

<!-- START_2ecd2c34871333ab0f1e6108147335fc -->
## Show the application dashboard.

> Example request:

```bash
curl -X GET \
    -G "https://api_laravel.test/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://api_laravel.test/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
null
```

### HTTP Request
`GET {any}`


<!-- END_2ecd2c34871333ab0f1e6108147335fc -->


