<?php

use App\Http\Controllers\Media\MediaController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
//Routes for users only
//Route::group(['middleware'=>['auth:api']],function(){
//
//});
//Routes for guests only
Route::group(['middleware' => ['guest:api']], function () {
    Route::post('login', 'Auth\LoginController@login');
    Route::post('register', 'Auth\RegisterController@register');
    Route::post('verification/verify/{user}', 'Auth\VerificationController@verify')->name('verification.verify');
    Route::post('verification/resend', 'Auth\VerificationController@resend');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetlinkEmail');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

});
Route::group(['middleware' => ['auth:api']], function () {
    Route::post('logout', 'Auth\LoginController@logout');
    Route::get('me', 'User\UserController@getMe');

    Route::get('users', 'User\UserController@index');
    Route::post('users/store', 'User\UserController@store');
    Route::post('users/update/{id}', 'User\UserController@update');
    Route::delete('users/destroy/{id}', 'User\UserController@destroy');
    /*---------------Profile----------------*/
    Route::put('setting/profile', 'Setting\SettingController@updateProfile');
    Route::put('setting/password', 'Setting\SettingController@updatePassword');
    Route::put('setting/avatar', 'Setting\SettingController@updateAvatar');


    /*---------------Design----------------*/
    Route::group(['prefix' => 'designs'], function () {
        Route::get('/', 'Design\DesignController@index');
        Route::get('detail/{id}', 'Design\DesignController@findDesign');
        Route::post('/upload', 'Design\UploadController@upload');
        Route::put('/{id}', 'Design\DesignController@update');
        Route::delete('/{id}', 'Design\DesignController@destroy');

        Route::get('search', 'Design\DesignController@search');
    });
    //Like
    Route::post('designs/{id}/like', 'Design\DesignController@like');
    Route::get('designs/{id}/liked', 'Design\DesignController@checkIfUserHasLiked');

    //Comment
    Route::post('designs/{id}/comments', 'Design\CommentController@store');
    Route::put('comments/{id}', 'Design\CommentController@update');
    Route::delete('comments/{id}', 'Design\CommentController@destroy');

    //Team
    Route::get('teams', 'Team\TeamController@index');
    Route::get('users/teams', 'Team\TeamController@fetchUserTeams');
    Route::post('teams', 'Team\TeamController@store');
    Route::get('teams/{id}', 'Team\TeamController@findById');
    Route::put('teams/{id}', 'Team\TeamController@update');
    Route::delete('teams/{id}', 'Team\TeamController@destroy');
    /*---------------User----------------*/
    Route::group(['prefix' => 'user'], function () {
        Route::get('/', 'Design\DesignController@index');
        Route::post('/upload', 'Design\UploadController@upload');
        Route::put('/{id}', 'Design\DesignController@update');
        Route::delete('/{id}', 'Design\DesignController@destroy');
    });

    /*---------------------Chats---------------------*/
    Route::post('chats', 'Chat\ChatController@sendMessage');
    Route::get('chats', 'Chat\ChatController@getUserChats');
    Route::get('chats/{id}/messages', 'Chat\ChatController@getChatMessages');
    Route::put('chats/{id}/markAsRead', 'Chat\ChatController@markAsRead');
    Route::delete('messages/{id}', 'Chat\ChatController@destroyMessage');
    Route::post('chats/real-time-message', 'Chat\ChatController@realMessage');

    Route::group(['prefix' => 'medias'], function () {
        Route::get('/{id}', [MediaController::class, 'index'])->name('medias.index');
        Route::post('/medias-folder/store', [MediaController::class, 'media\MediaController@storeFolder'])->name('medias.folder.store');
        Route::post('/medias-file/store/{id}', [MediaController::class, 'uploadFile'])->name('medias.upload');
        Route::post('/medias-rename/update', [MediaController::class, 'renameMedia'])->name('medias.rename');
        Route::post('/medias-move/trash', [MediaController::class, 'moveTrashMedia'])->name('medias.move.trash');
        Route::get('/medias/download/{id}', [MediaController::class, 'downloadMedia'])->name('medias.download');
        Route::post('/medias/delete', [MediaController::class, 'deleteMedia'])->name('medias.delete');
        Route::post('/medias/restore', [MediaController::class, 'restoreMedia'])->name('medias.restore');
    });
});

