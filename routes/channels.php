<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Broadcast;


/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/


//Broadcast::channel('chat-channel.{id}', function ($id) {
//    return true;
//});

Broadcast::channel('chat-channel', function ($user) {
    return Auth::check();
});



Broadcast::channel('online.{id}', function ($user) {

    return auth()->check();
});
