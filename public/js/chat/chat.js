/**
 *-------------------------------------------------------------
 * Global variables
 *-------------------------------------------------------------
 */
var messenger,
    auth_id = $('meta[name=url]').attr('data-user'),
    route = $('meta[name=route]').attr('content'),
    url = $('meta[name=url]').attr('content'),
    access_token = $('meta[name="csrf-token"]').attr('content'),
    typingTimeout,
    typingNow = 0,
    temporaryMsgId = 0,
    defaultAvatarInSettings = null,
    messengerColor,
    dark_mode;
const messagesContainer = $('.messenger-messagingView .m-body'),
    messengerTitleDefault = $('.messenger-headTitle').text(),
    messageInput = $('#message-form .m-send');
// console.log(auth_id);

// Loading svg
function loadingSVG(w_h = '25px', className = null) {
    return `
    <svg class="loadingSVG `+ className + `" xmlns="http://www.w3.org/2000/svg" width="` + w_h + `" height="` + w_h + `" viewBox="0 0 40 40" stroke="#2196f3">
      <g fill="none" fill-rule="evenodd">
        <g transform="translate(2 2)" stroke-width="3">
          <circle stroke-opacity=".1" cx="18" cy="18" r="18"></circle>
          <path d="M36 18c0-9.94-8.06-18-18-18" transform="rotate(349.311 18 18)">
              <animateTransform attributeName="transform" type="rotate" from="0 18 18" to="360 18 18" dur=".8s" repeatCount="indefinite"></animateTransform>
          </path>
        </g>
      </g>
    </svg>
    `;
}

// upload image preview card.
function attachmentTemplate(fileType, fileName, imgURL = null) {
    if (fileType != 'image') {
        return `
        <div class="attachment-preview">
            <span class="fas fa-times cancel"></span>
            <p style="padding:0px 30px;"><span class="fas fa-file"></span> `+ fileName + `</p>
        </div>
        `;
    } else {
        return `
        <div class="attachment-preview">
            <span class="fas fa-times cancel"></span>
            <div class="image-file chat-image" style="background-image: url(`+ imgURL + `);"></div>
            <p><span class="fas fa-file-image"></span> `+ fileName + `</p>
        </div>
        `;
    }
}

$(document).ready(function () {
    // NProgress configurations
    NProgress.configure({ showSpinner: false, minimum: 0.7, speed: 500 });
    // tabs on click, show/hide...
    $('.messenger-listView-tabs a').on('click', function () {
        var dataView = $(this).attr('data-view');
        $('.messenger-listView-tabs a').removeClass('active-tab');
        $(this).addClass('active-tab');
        $('.messenger-tab').hide();
        $('.messenger-tab[data-view=' + dataView + ']').show();
    });

    // set item active on click
    $('body').on('click', '.messenger-list-item', function () {
        $('.messenger-list-item').removeClass('m-list-active');
        $(this).addClass('m-list-active');
    });



    // show info side button
    $('.messenger-infoView nav a , .show-infoSide').on('click', function () {
        $('.messenger-infoView').toggle();
    });

    // x button for info section to show the main button.
    $('.messenger-infoView nav a').on('click', function () {
        $('.show-infoSide').show();
    });

    // hide showing button for info section.
    $('.show-infoSide').on('click', function () {
        $(this).hide();
    });


    // list view buttons
    $('.listView-x').on('click', function () {
        $('.messenger-listView').hide();
    });
    $('.show-listView').on('click', function () {
        $('.messenger-listView').show();
    });

    // click action for [add to favorite] button.
    $('.add-to-favorite').on('click', function () {
        star(messenger.split('_')[1]);
    });

    // Image modal
    $('body').on('click', ".chat-image", function () {
        let src = $(this).css("background-image").split(/"/)[1];
        $("#imageModalBox").show();
        $("#imageModalBoxSrc").attr('src', src);
    });
    $('.imageModal-close').on('click', function () {
        $("#imageModalBox").hide();
    });

    // Search input on focus
    $('.messenger-search').on('focus', function () {
        $('.messenger-tab').hide();
        $('.messenger-tab[data-view="search"]').show();
    });
    // Settings button action to show settings modal
    $('.settings-btn').on('click', function () {
        app_modal({
            name: 'settings',
        });
    });

    // on submit settings' form
    $('#updateAvatar').on('submit', (e) => {
        e.preventDefault();
        updateSettings();
    });
    // Settings modal [cancel button]
    $('.app-modal[data-name=settings]').find('.app-modal-footer .cancel').on('click', function () {
        app_modal({
            show: false,
            name: 'settings',
        });
        cancelUpdatingAvatar();
    });

    /**
     *-------------------------------------------------------------
     * App Modal
     *-------------------------------------------------------------
     */
    let app_modal = function ({show = true, name, data = 0, buttons = true, header = null, body = null,}) {
        const modal = $('.app-modal[data-name=' + name + ']');
        // header
        header ? modal.find('.app-modal-header').html(header) : '';

        // body
        body ? modal.find('.app-modal-body').html(body) : '';

        // buttons
        buttons == true
            ? modal.find('.app-modal-footer').show()
            : modal.find('.app-modal-footer').hide();

        // show / hide
        if (show == true) {
            modal.show();
            $('.app-modal-card[data-name=' + name + ']').addClass('app-show-modal');
            $('.app-modal-card[data-name=' + name + ']').attr('data-modal', data);
        } else {
            modal.hide();
            $('.app-modal-card[data-name=' + name + ']').removeClass('app-show-modal');
            $('.app-modal-card[data-name=' + name + ']').attr('data-modal', data);
        }

    };
    /**
     *-------------------------------------------------------------
     * Cancel updating avatar in settings
     *-------------------------------------------------------------
     */
    function cancelUpdatingAvatar() {
        $('.upload-avatar-preview').css('background-image', defaultAvatarInSettings);
        $('.upload-avatar').replaceWith($('.upload-avatar').val('').clone(true));
    }
    // upload avatar on change
    $('body').on('change', ".upload-avatar", (e) => {
        // store the original avatar
        if (defaultAvatarInSettings == null) {
            defaultAvatarInSettings = $('.upload-avatar-preview').css('background-image');
        }
        let file = e.target.files[0];
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.addEventListener('loadstart', (e) => {
            $('.upload-avatar-preview').append(loadingSVG('42px', 'upload-avatar-loading'));
        });
        reader.addEventListener('load', (e) => {
            $('.upload-avatar-preview').find('.loadingSVG').remove();
            if (!file.type.match("image.*")) {
                // if the file is not an image
                console.error('File you selected is not an image!');
            } else {
                // if the file is an image
                $('.upload-avatar-preview').css('background-image', 'url("' + e.target.result + '")');
            }
        });
    });

    function updateSettings() {
        const formData = new FormData($("#updateAvatar")[0]);
        if (messengerColor) {
            formData.append('messengerColor', messengerColor);
        }
        if (dark_mode) {
            formData.append('dark_mode', dark_mode);
        }
        $.ajax({
            url:'/updateSettings',
            method: 'POST',
            data: formData,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: () => {
                // close settings modal
                app_modal({
                    show: false,
                    name: 'settings',
                });
                // Show waiting alert modal
                app_modal({
                    show: true,
                    name: 'alert',
                    buttons: false,
                    body: loadingSVG('32px'),
                });
            },
            success: (data) => {
                if (data.error) {
                    // Show error message in alert modal
                    app_modal({
                        show: true,
                        name: 'alert',
                        buttons: true,
                        body: data.msg,
                    });
                } else {
                    // Hide alert modal
                    app_modal({
                        show: false,
                        name: 'alert',
                        buttons: true,
                        body: '',
                    });

                    // reload the page
                    location.reload(true);
                }
            },
            error: () => {
                console.error('Server error, check your response');
            }
        });
    }

    // On [upload attachment] input change, show a preview of the image/file.
    $('body').on('change', ".upload-attachment", (e) => {
        let file = e.target.files[0];
        let reader = new FileReader();
        let sendCard = $('.messenger-sendCard');
        reader.readAsDataURL(file);
        reader.addEventListener('loadstart', (e) => {
            $('#message-form').before(loadingSVG());
        });
        reader.addEventListener('load', (e) => {
            $('.messenger-sendCard').find('.loadingSVG').remove();
            if (!file.type.match("image.*")) {
                // if the file not image
                sendCard.find('.attachment-preview').remove(); // older one
                sendCard.prepend(attachmentTemplate('file', file.name));
            } else {
                // if the file is an image
                sendCard.find('.attachment-preview').remove(); // older one
                sendCard.prepend(attachmentTemplate('image', file.name, e.target.result));
            }
        });
    });

    // Attachment preview cancel button.
    $('body').on('click', ".attachment-preview .cancel", (e) => {
        cancelAttachment();
    });
    /**
     *-------------------------------------------------------------
     * Cancel file attached in the message.
     *-------------------------------------------------------------
     */
    function cancelAttachment() {
        $('.messenger-sendCard').find('.attachment-preview').remove();
        $('.upload-attachment').replaceWith($('.upload-attachment').val('').clone(true));
    }

});
